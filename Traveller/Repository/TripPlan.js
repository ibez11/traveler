const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID
const Validator = require('node-input-validator');
const DateFormatter = require("../../lib/Modules/DateFormatter");

const AbstractRepository = require("./AbstractRepository");

const TripPlanModel = require("../../models/TripPlanModel").TripPlan;

class TripPlan extends AbstractRepository {
    constructor(model = new TripPlanModel()) {
    }
}

module.exports = TripPlan;