"use strict";
const {ObjectId} = require('mongodb'); // or ObjectID
const WorkerModel = require("../../../models/WorkerModel").Worker;
require('dotenv').config();

class WorkerFinder {
    
    constructor(param) {
        this.page = 1;
        this.per_page = 15;
        this.param = param;
        this.pipeline = [
        {
            $lookup: {
                from: 'workerCategoryLinks',
                localField: '_id',
                foreignField: 'worker_id',
                as: 'category_link_detail',
            }
        },
        {
            $lookup: {
                from: 'categories',
                localField: 'category_link_detail.category_id',
                foreignField: '_id',
                as: 'category_detail'
            }
        }
    ];
        
        this.query = WorkerModel;
    }

    async orderBy(columnName, orderBy)
    {
        switch(columnName) {
            case 'category_detail.label':
                this.pipeline.push({$sort: { 'category_detail.label': orderBy == 'desc' ? -1 : 1 }});
                break;
            case 'fullname':
                this.pipeline.push({$sort: { 'fullname': orderBy == 'asc' ? -1 : 1 }});
                break;
            case 'no_hp':
                this.pipeline.push({$sort: { no_hp: orderBy == 'asc' ? -1 : 1 }});
                break;
            default:
                this.pipeline.push({$sort: { created_at : -1 }});
                break;
        }
}

    setPerPage(per_page)
    {
        this.per_page = per_page;
    }

    getPerPage()
    {
        return this.per_page;
    }

    async setPage(page)
    {
        this.page = page;
    }

    async getPage()
    {
        return this.page;
    }

    setKeyword(keyword)
    {
        if(keyword) {
            let query = [];
            // Split keyword first
            let listKeyword = keyword.split(" ");
            listKeyword = listKeyword.map(function(elem){
                return elem.trim();
            });;

            let columnList = [];
            let pattern = '';
            listKeyword.forEach(keyword => {
                pattern = `.*${keyword}.*`;
                columnList.push('category_detail.label', 
                'fullname', 
                'no_hp'
                );
            })

            columnList.forEach(x => {
                query.push(
                    { [x]: { $regex: pattern, $options: 'si'} }
                );
            })
            
            this.pipeline.push({$match: {$or: query}});
        }
    }

    setStatus(status)
    {
        this.pipeline.push({$match: {is_actived: status}});
    }

    async get()
    {
        let query = this.query;
        let pipeline = this.pipeline;

        pipeline.push({$facet: {
            totalCount: [
              {
                $count: 'count'
              }
            ]
        }});
        var count = await query.aggregate(pipeline).exec();
        count = count[0].totalCount.length ? parseInt(count[0].totalCount[0].count) : 0;
        pipeline.pop();
        
        switch(this.page) {
            case 'all':
                return new Promise( async function (fulfilled, rejected) {
                    query.aggregate(pipeline).exec(function(err, docs) {
                        if (err) {
                            console.log(err)
                        } else {
                            var data = {
                                data: docs,
                                total: docs.length,
                                total_page: 1
                            }
                            
                            fulfilled(data)
                        }
                    });
                });
            default:
                let page = parseInt(this.page);
                let perPage = parseInt(this.per_page);
                
                return new Promise( async function (fulfilled, rejected) {
                    pipeline.push({
                        $skip: page > 0 ? ((page - 1) * perPage) : 0
                    });
                    pipeline.push({$limit: perPage});
                    query.aggregate(pipeline).exec(function(err, docs) {
                        if (err) {
                            console.log(err)
                        } else {
                            var data = {
                                data: docs,
                                current_page: page,
                                last_page: Math.ceil(parseInt(count)/perPage),
                                per_page: perPage,
                                total: count,
                                total_page: Math.ceil(parseInt(count)/perPage)
                            }
                            
                            fulfilled(data)
                        }
                    });
                });
        }
    }
}

module.exports = WorkerFinder;