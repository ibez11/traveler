"use strict";
const {ObjectId} = require('mongodb'); // or ObjectID
const DateFormatter = require("../../../lib/Modules/DateFormatter");
const TravellerModel = require("../../../models/TravellerModel").Traveller;
require('dotenv').config();

class TravellerFinder {
    
    constructor(param) {
        this.page = 1;
        this.per_page = 15;
        this.param = param;
        this.pipeline = [
        {
            $lookup: {
                from: 'categories',
                localField: 'category_id',
                foreignField: '_id',
                as: 'category_detail'
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user_detail'
            }
        },
        {
            $lookup: {
                from: 'bidders',
                localField: '_id',
                foreignField: 'Traveller_id',
                as: 'bidder_detail'
            }
        },
        {
            $lookup: {
                from: 'workers',
                localField: 'bidder_detail.worker_id',
                foreignField: '_id',
                as: 'worker_detail'
            }
        }
    ];
        
        this.query = TravellerModel;
    }

    async orderBy(columnName, orderBy)
    {
        switch(columnName) {
            case 'user_detail.fullname':
                this.pipeline.push({$sort: { 'user_detail.fullname': orderBy == 'desc' ? -1 : 1 }});
                break;
            case 'budget':
                this.pipeline.push({$sort: { 'budget': orderBy == 'asc' ? -1 : 1 }});
                break;
            case 'category_detail.label':
                this.pipeline.push({$sort: { 'category_detail.fullname': orderBy == 'asc' ? -1 : 1 }});
                break;
            default:
                this.pipeline.push({$sort: { created_at : -1 }});
                break;
        }
}

    setPerPage(per_page)
    {
        this.per_page = per_page;
    }

    getPerPage()
    {
        return this.per_page;
    }

    async setPage(page)
    {
        this.page = page;
    }

    async getPage()
    {
        return this.page;
    }

    setKeyword(keyword)
    {
        if(keyword) {
            let query = [];
            // Split keyword first
            let listKeyword = keyword.split(" ");
            listKeyword = listKeyword.map(function(elem){
                return elem.trim();
            });;

            let columnList = [];
            let pattern = '';
            listKeyword.forEach(keyword => {
                pattern = `.*${keyword}.*`;
                columnList.push('user_detail.fullname', 
                'category_detail.label', 
                'budget'
                );
            })

            columnList.forEach(x => {
                query.push(
                    { [x]: { $regex: pattern, $options: 'si'} }
                );
            })
            
            this.pipeline.push({$match: {$or: query}});
        }
    }

    setStatus(status)
    {
        this.pipeline.push({$match: {status: status}});
    }

    setUser(userId)
    {
        this.pipeline.push({$match: {user_id: ObjectId(userId)}});
    }

    setWorker(workerId)
    {
        this.pipeline.push({$match: {'worker_detail._id': ObjectId(workerId)}});
    }

    setCategory(categoryId)
    {
        this.pipeline.push({$match: {'category_detail._id': categoryId}});
    }

    setOneHours()
    {
        const dateFormatter = new DateFormatter();
        var before = dateFormatter.dateBefore("YYYY/MM/DD HH:mm:ss", 60);
        var now = dateFormatter.dateCustom(new Date(), "YYYY/MM/DD HH:mm:ss");
        
        this.pipeline.push({$match: {'created_at': {"$gte": before, "$lte": now}}});
    }

    async get()
    {
        let query = this.query;
        let pipeline = this.pipeline;

        pipeline.push({$facet: {
            totalCount: [
              {
                $count: 'count'
              }
            ]
        }});
        var count = await query.aggregate(pipeline).exec();
        count = count[0].totalCount.length ? parseInt(count[0].totalCount[0].count) : 0;
        pipeline.pop();
        
        switch(this.page) {
            case 'all':
                return new Promise( async function (fulfilled, rejected) {
                    query.aggregate(pipeline).exec(function(err, docs) {
                        if (err) {
                            console.log(err)
                        } else {
                            var data = {
                                data: docs,
                                total: docs.length,
                                total_page: 1
                            }
                            
                            fulfilled(data)
                        }
                    });
                });
            default:
                let page = parseInt(this.page);
                let perPage = parseInt(this.per_page);
                
                return new Promise( async function (fulfilled, rejected) {
                    pipeline.push({
                        $skip: page > 0 ? ((page - 1) * perPage) : 0
                    });
                    pipeline.push({$limit: perPage});
                    query.aggregate(pipeline).exec(function(err, docs) {
                        if (err) {
                            console.log(err)
                        } else {
                            var data = {
                                data: docs,
                                current_page: page,
                                last_page: Math.ceil(parseInt(count)/perPage),
                                per_page: perPage,
                                total: count,
                                total_page: Math.ceil(parseInt(count)/perPage)
                            }
                            
                            fulfilled(data)
                        }
                    });
                });
        }
    }
}

module.exports = TravellerFinder;