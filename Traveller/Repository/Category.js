const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID

const AbstractRepository = require("./AbstractRepository");

const CategoryModel = require("../../models/CategoryModel").Category;

class CategoryLeave extends AbstractRepository {
    constructor(model = new CategoryModel) {
        super(model);
    }

    async save() 
    {
        var result = [];
        
        try {
            mongoose.set('useFindAndModify', false);
            let params = this.model.toObject();
            
            var currentId;
            if (params._id) {
                currentId = params._id;
                delete params._id;
            } else {
                var getLastId = await CategoryModel.findOne({}).sort({_id:-1}).limit(1);
                currentId = getLastId._id + 1;
            }

            let data = {};
            
            return new Promise( function (fulfilled, rejected) {
                CategoryModel.findOneAndUpdate({_id: currentId}, params, { upsert: true, new: true }, (err, doc) => {
                    
                    if(err) {
                        rejected(err)
                    }

                    if(doc){
                        data = doc['_doc'];

                        result['status'] = 1;
                        result['message'] = 'Success';
                        result['data'] = data
                        
                        fulfilled(result)
                    }
                    
                });
            });
        } catch(e) {
            console.log(e)
            result['status'] = 0;
            result['message'] = e.message;
            
        }
        
        return result;
    }
}

module.exports = CategoryLeave;