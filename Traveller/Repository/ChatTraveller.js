const AbstractRepository = require("../Repository/AbstractRepository");
const Validator = require('node-input-validator');


const ChatTravellerModel = require("../../models/ChatTravellerModel").ChatTraveller;

class ChatTraveller extends AbstractRepository {
    constructor(model = new ChatTravellerModel) {
        super(model);
    }

    async save()
    {
        let validator = null;
        
        Validator.addCustomMessages({
            'message.required': 'Message kosong',
        });

        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });

        // Validation
        let fields = {
            message: this.model.message
        };

        let rules = {
            message: 'required'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);

        this.model.save();
    }
}

module.exports = ChatTraveller;