const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const {ObjectId} = require('mongodb'); // or ObjectID
const serviceAccount = require("../../key/key.json");

const PasswordGenerate = require("../../ThebadusLibs/BadusPasswordGenerate");

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const AbstractRepository = require("../Repository/AbstractRepository");
const WorkerModel = require("../../models/WorkerModel").Worker;
const WorkerCategoryLinkModel = require("../../models/WorkerCategoryLinkModel").WorkerCategoryLink;
const WorkerLoggedModel = require("../../models/WorkerLoggedModel").workerLogged;

const Validator = require('node-input-validator');

class Worker extends AbstractRepository {
    constructor(model = new WorkerModel) {
        super(model);
        this.workerCategoryLink = [];
    }

    async addCategoryLink(param)
    {
        this.workerCategoryLink.push(param);
    }

    async validate() 
    {
        let validator = null;
        
        Validator.extend('in', async function ({ value }) {
            if( value == true )
                return true;
            
            return false;
        });
        
        Validator.addCustomMessages({
            'username.required': 'Username harus diisi.',
            'password.required': 'Password harus diisi',
            'matched.in': 'Username atau Password salah',
            'is_actived.in': 'User tidak aktif lagi'
        });

        var detailWorker = await this.getDetailWorker(this.model.username)
        var check = false;
        var is_actived = false;
        
        if(detailWorker) {
            var pw = detailWorker.password;
            is_actived = detailWorker.is_actived;
            check = bcrypt.compareSync(this.model.password, pw);
        }
        
        var fields = {
            username: this.model.username,
            matched: check,
            is_actived: is_actived,
            password: this.model.password,
        };
        
        var rules = {
            username:'required',
            password: 'required',
            is_actived: 'in:true,false',
            matched: 'in:true,false',
        }

        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);
    }

    async signIn(ip) {
        var result = [];

        await this.validate()
        
        var detailWorker = await this.getDetailWorker(this.model.username);
        
        const payload = {
            check: true,
            user_id: detailWorker._id,
            ip: ip
        };

        const token = jwt.sign(payload, serviceAccount.private_key, { algorithm: 'RS256', header: {
            kid: serviceAccount.private_key_id
        }, expiresIn: '356d'});
        
        let setParamData = {
            jwt: token,
            user_id: detailWorker._id,
            username: detailWorker.username
        }
        
        await this.setWorkerLogged(setParamData);

        result['token'] = token;
        result['message'] = 'Anda berhasil login';

        return result;
    }

    async getDetailWorker(username) 
    {
        var x = new Promise( function (fulfilled, rejected) {
            WorkerModel.findOne({username: username}, function(err, data) {
                if(err) rejected(err)
                
                if(data) {
                    let opts = [{
                        path: 'category_detail',
                        model: WorkerCategoryLinkModel
                    }];
                    
                    WorkerModel.populate(data, opts, function(err, data) {
                        if(err)
                        rejected(err)
                        
                        data['_doc'].role_type_code = data.role_type_code;
                        data['_doc'].employee_info = data.employee_info;
                        fulfilled(data['_doc']);
                    })
                } else {
                    fulfilled(null);
                }
            })
        });

        return x;
    }

    async setWorkerLogged(param) 
    {
        var dateFormatter = new DateFormatter();
        var now = dateFormatter.date(date);

        await WorkerLoggedModel.deleteMany({ user_id: param.user_id }, function(err) {
            if (err) {
                console.log(err);
            }
        });
        var userLoggedModel = new WorkerLoggedModel;
        // userLoggedModel._id = ObjectId(param.user_id);
        userLoggedModel.user_id = param.user_id;
        userLoggedModel.username = param.username;
        userLoggedModel.role_type = param.role_type;
        userLoggedModel.jwt_encrypt = param.jwt;
        userLoggedModel.created_at = now;
        
        userLoggedModel.save({_id: ObjectId(param.user_id)}, (err, doc) => {
            if(err)
            console.log(err)
        });
    }

    async getWorkerLogged(param) 
    {
        let isLogged = await new Promise(async function (fulfilled, rejected) {
            let result = null;
            
            let row = await WorkerLoggedModel.findOne({user_id: ObjectId(param.user_id)}).populate({
                path: 'worker',
                momdel: WorkerModel,
                populate: {
                    path: 'category_detail',
                    model: WorkerCategoryLinkModel
                }
            }).exec();

            if(row && row.worker.length) {
                row.worker[0].category_detail.forEach(x => {
                    row.worker[0].category_detail = [];
                    row.worker[0].category_detail.push(x.category_id);
                })
                result = row;
            }
            
            fulfilled(result);
        });
       
        
        return isLogged;
    }

    async save()
    {
        let validator = null;
        
        var checkUsername = await WorkerModel.countDocuments({username: this.model.username}).exec();
        
        Validator.addCustomMessages({
            'password_length.minLength': 'Password minimum 6',
            'password_length.required': 'Password harus diisi',
            'username.required': 'Username harus diisi',
            'username_already_exist.in': 'Username sudah terdaftar',
            'fullname.required': 'Fullname harus diisi',
            'category_length.in': 'Category minimal 1'
        });
        
        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });

        // Validation
        let fields = {
            password_length: this.model.password,
            username: this.model.username,
            username_already_exist: checkUsername == 0,
            fullname: this.model.fullname,
            category_length: this.workerCategoryLink.length > 0 && this.workerCategoryLink.length <= 3
        };
        
        let rules = {
            password_length: 'required|minLength:5',
            username: 'required',
            username_already_exist: 'in:true,false',
            fullname: 'required',
            category_length: 'in:true,false'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);

        this.model.password = PasswordGenerate(this.model.password);
        this.model.save();

        this.workerCategoryLink.forEach(async x => {
            var workerCategoryLinkModel = new WorkerCategoryLinkModel(x);
            
            workerCategoryLinkModel._id = ObjectId();
            
            await workerCategoryLinkModel.save();
        })
    }

    async setStatus()
    {
        this.model.save();
    }
}

module.exports = Worker;