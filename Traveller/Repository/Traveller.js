const mongoose = require("mongoose");
const {ObjectId} = require('mongodb'); // or ObjectID
const Validator = require('node-input-validator');
const DateFormatter = require("../../lib/Modules/DateFormatter");

const AbstractRepository = require("./AbstractRepository");

const TravellerModel = require("../../models/TravellerModel").Traveller;
const Status = require("../../models/TravellerModel").TravellerStatuses;
const BidderModel = require("../../models/BidderModel").Bidder;
const TravellerCommentWorkerModel = require("../../models/TravellerCommentWorkerModel").TravellerCommentWorker;

class Traveller extends AbstractRepository {
    constructor(model = new TravellerModel()) {
        super(model);
        this.userModel = null;
        this.bidderModel = null;
        this.TravellerCommentWorkerModel = null;
    }

    async addBidder(model = new BidderModel)
    {
        this.bidderModel = model;
    }

    async addTravellerCommentWorkerModel(model = new TravellerCommentWorkerModel)
    {
        this.TravellerCommentWorkerModel = model;
    }

    async validate()
    {
        let validator = null;
        
        Validator.addCustomMessages({
            'title.required': 'Title harus diisi',
            'description.required': 'Description harus diisi',
            'category_id.required': 'Category id harus diisi',
            'budget.min': 'Budget min 100 max 999999999',
            'budget.required': 'Budget harus diisi',
        });

        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });

        // Validation
        let fields = {
            title: this.model.title,
            description: this.model.description,
            category_id: this.model.category_id,
            budget: this.model.budget
        };

        let rules = {
            title: 'required',
            description: 'required',
            category_id: 'required',
            budget: 'required|numeric|min:100|max:999999999'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);
        
    }

    async save() 
    {
        var result = [];

        await this.validate();

        mongoose.set('useFindAndModify', false);
        let param = this.model.toObject();
        
        var currentId;
        if (param._id) {
            currentId = param._id;
            delete param._id;
        } else {
            currentId = ObjectId();
        }

        let data = {};
        
        return new Promise( function (fulfilled, rejected) {
            TravellerModel.findOneAndUpdate({_id: currentId}, param, { upsert: true, new: true }, (err, doc) => {
                if(err) {
                    rejected(err)
                }

                if(doc){
                    data = doc['_doc'];

                    result['status'] = 1;
                    result['message'] = 'Success';
                    result['data'] = data
                    
                    fulfilled(result)
                }
                
            });
        });
    }

    async saveBidder(statusId)
    {
        let validator = null;
        
        Validator.addCustomMessages({
            'Traveller_status.in': 'Traveller status tidak waiting'
        });

        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });
        
        // Validation
        let fields = {
            Traveller_status: statusId == Status.STATUS_WAITING
        };

        let rules = {
            Traveller_status: 'in:true,false'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);

        this.bidderModel.save();
        this.model.save();
    }

    async saveFinish()
    {
        let validator = null;
        
        Validator.addCustomMessages({
            'Traveller_status.in': 'Traveller status masih Waiting'
        });

        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });

        // Validation
        let fields = {
            Traveller_status: this.model.status == Status.STATUS_PROGRESS
        };

        let rules = {
            Traveller_status: 'in:true,false'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);

        this.model.save();
    }

    async saveTravellerCommentWorker()
    {
        let validator = null;
        
        Validator.addCustomMessages({
            'Traveller_status.in': 'Traveller status belum selesai'
        });

        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });

        // Validation
        let fields = {
            Traveller_status: this.model.status == Status.STATUS_FINISH
        };

        let rules = {
            Traveller_status: 'in:true,false'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);
        
        this.TravellerCommentWorkerModel.save();
    }

    async canceledAfterOneHour()
    {
        const dateFormatter = new DateFormatter();
        var before = dateFormatter.dateBefore("YYYY/MM/DD HH:mm:ss", 60);
        var now = dateFormatter.dateCustom(new Date(), "YYYY/MM/DD HH:mm:ss");

        await TravellerModel.updateMany({created_at: {"$gte": before, "$lte": now}}, {status: Status.STATUS_CANCELED}).exec();
    }
}

module.exports = Traveller;