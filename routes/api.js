var express = require("express");
var router = express.Router();

const { TravellerController } = require('../controllers/TravellerController');
const { WorkerController } = require('../controllers/WorkerController');
const { CategoryController } = require('../controllers/CategoryController');
const { ChatTravellerController } = require('../controllers/ChatTravellerController');

const { auth } = require('../controllers/AuthController');
const { Auth } = require('../middleware/Auth');

// Category
router.get('/master/category/list', CategoryController.index);
router.get('/master/category/detail/:id', CategoryController.show);

// Auth User
router.post('/user/login', auth.signInUser);
router.post('/user/register', auth.registerUser);

// Auth Worker
router.post('/worker/login', auth.signInWorker);
router.post('/worker/register', auth.registerWorker);

// Worker List - User
router.get('/master/user/worker-list', Auth.middleware(['user']).isAuth, WorkerController.index);
router.get('/master/user/worker-detail/:id', Auth.middleware(['user']).isAuth, WorkerController.show);
router.post('/master/user/worker-create', Auth.middleware(['user']).isAuth, WorkerController.store);
router.post('/master/user/worker-set-isactived/:id', Auth.middleware(['user']).isAuth, WorkerController.setStatusIsActived);

// traveller User
router.get('/master/user/traveller/list', Auth.middleware(['user']).isAuth, TravellerController.index);
router.get('/master/user/traveller/detail/:id', Auth.middleware(['user']).isAuth, TravellerController.show);
router.post('/master/user/traveller/create', Auth.middleware(['user']).isAuth, TravellerController.store);

// Worker traveller Comment
router.post('/master/user/traveller/comment-traveller-worker/:id', Auth.middleware(['user']).isAuth, TravellerController.commentTravellerWorker);

// traveller Worker
router.get('/master/worker/traveller/bid-list', Auth.middleware(['worker']).isAuth, TravellerController.bidWorker);
router.get('/master/worker/traveller/list', Auth.middleware(['worker']).isAuth, TravellerController.index);
router.get('/master/worker/traveller/detail/:id', Auth.middleware(['worker']).isAuth, TravellerController.show);
router.post('/master/worker/traveller/bid/:id', Auth.middleware(['worker']).isAuth, TravellerController.setBid);
router.post('/master/worker/traveller/finish/:id', Auth.middleware(['worker']).isAuth, TravellerController.finish);

// Chat
router.get('/master/worker/traveller-message-list/:id', Auth.middleware(['worker']).isAuth, ChatTravellerController.index);
router.get('/master/user/traveller-message-list/:id', Auth.middleware(['user']).isAuth, ChatTravellerController.index);

router.post('/master/worker/traveller-send-message/:id', Auth.middleware(['worker']).isAuth, ChatTravellerController.sendMessageWorker);
router.post('/master/user/traveller-send-message/:id', Auth.middleware(['user']).isAuth, ChatTravellerController.sendMessageUser);


// Canceled traveller After One Hours
router.post('/master/worker/traveller/canceled-after-one-hours/:id', Auth.middleware(['worker']).isAuth, TravellerController.setCancelAfteOneHours);

router.get("/", function(req,res) {
    res.status(500).send({
        success: false,
        message: "Maaf data tidak tersedia"
    })
});

module.exports = router;