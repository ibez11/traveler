'use strict';
const table = 'workers';
const {mongoDB} = require('./Db')

mongoDB.set('useCreateIndex', true);
var Schema = mongoDB.Schema;
var CurrentSchema = new Schema({
    fullname: {type: String, default: ''},
    no_hp: {type: String, default: ''},
    username: {type: String, index: true, unique: true},
    user_type: {type: String, default: 'worker'},
    password: {type: String, default: ''},
    description: {type: String, default: ''},
    is_actived: {type: Boolean, default: true},
    created_at: {type: String, default: ''}
});

CurrentSchema.virtual('category_detail',{
    ref: 'workerCategoryLinks',
    localField: '_id',
    foreignField: 'worker_id'
});

CurrentSchema.set('toObject', { virtuals: true });
CurrentSchema.set('toJSON', { virtuals: true });


var CurrentModel = mongoDB[process.env.DB_NAME].model(table, CurrentSchema, table);
module.exports.Worker = CurrentModel;