
'use strict';
const table = 'balance';
var {mongoDB} = require("./Db");

var Schema = mongoDB.Schema;
var BalanceSchema = new Schema({
    email: {type: String},
    balance: { type: Number, default: 0 },
    created_at: {type: String, default: ''},
    updated_at: {type: String, default: ''}
});

var Balance = mongoDB[process.env.DB_NAME].model(table, BalanceSchema, table);
module.exports.Balance = Balance;