'use strict';
var {mongoDB} = require("./Db");
var table = 'Travellers';

class TravellerStatuses {
    constructor(){
        this.STATUS_PROGRESS = 28;
        this.STATUS_CANCELED = 29;
        this.STATUS_WAITING = 30;
        this.STATUS_FINISH = 31;

        this.PAYMENT_METHOD_CASH = 0;
        this.PAYMENT_METHOD_WALLET = 1;
    }

    getStatusIdByName(name)
    {
        let statusId = null;

        switch(name) {
            case 'progress':
                statusId = this.STATUS_PROGRESS;
                break;

            case 'canceled':
                statusId = this.STATUS_CANCELED;
                break;

            case 'waiting':
                statusId = this.STATUS_WAITING;
                break;

            case 'finish':
                statusId = this.STATUS_FINISH;
                break;
        }

        return statusId;
    }
}

let travellerStatuses = new TravellerStatuses;
module.exports.TravellerStatuses = travellerStatuses;

var Schema = mongoDB.Schema;
var TravellerSchema = new Schema({
    from: {type: String, default: ''},
    to: {type: String, default: ''},
    pic: {type: String, default: ''},
    note: {type: String, default: ''},
    available_space: {type: String, default: ''},
    depart_date_and_time: {type: String, default: ''},
    move_by: {type: String, default: ''},
    additional_description: {type: String, default: ''},
    title: {type: String, default: ''},
    description: {type: String, default: ''},
    category_id: {type: Number, default: null},
    user_id: {type: Schema.Types.ObjectId, default: null},
    status: {type: Number, default: 30},
    budget: {type: Number, default: 0},
    number_of_worker: {type: Number, default: 0},
    is_specific_budget: {type: Boolean, default: false},
    transaction_no: {type: String, default: ''},
    payment_method: {type: Number, default: 0},
    created_at: {type: String, default: ''},
    updated_at: {type: String, default: ''}
});

TravellerSchema.virtual('bid_info',{
    ref: 'bidders',
    localField: '_id',
    foreignField: 'Traveller_id',
    default: {},
    justOne: true
});

TravellerSchema.virtual('category_info',{
    ref: 'categories',
    localField: 'category_id',
    foreignField: '_id',
    default: {},
    justOne: true
});

TravellerSchema.set('toObject', { virtuals: true });
TravellerSchema.set('toJSON', { virtuals: true });

var traveller = mongoDB[process.env.DB_NAME].model(table, TravellerSchema, table);
module.exports.Traveller = traveller;