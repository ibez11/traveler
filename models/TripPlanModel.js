'use strict';
var {mongoDB} = require("./Db");
var table = 'Travellers';

class TripPlanStatuses {
    constructor(){
        this.STATUS_PROGRESS = 28;
        this.STATUS_CANCELED = 29;
        this.STATUS_WAITING = 30;
        this.STATUS_FINISH = 31;

        this.PAYMENT_METHOD_CASH = 0;
        this.PAYMENT_METHOD_WALLET = 1;
    }

    getStatusIdByName(name)
    {
        let statusId = null;

        switch(name) {
            case 'progress':
                statusId = this.STATUS_PROGRESS;
                break;

            case 'canceled':
                statusId = this.STATUS_CANCELED;
                break;

            case 'waiting':
                statusId = this.STATUS_WAITING;
                break;

            case 'finish':
                statusId = this.STATUS_FINISH;
                break;
        }

        return statusId;
    }
}

let TravellerStatuses = new TripPlanStatuses;
module.exports.TripPlanStatuses = TravellerStatuses;

var Schema = mongoDB.Schema;
var TripPlanSchema = new Schema({
    from: {type: String, default: ''},
    to: {type: String, default: ''},
    title: {type: String, default: ''},
    pic: {type: String, default: ''},
    note: {type: String, default: ''},
    available_space: {type: String, default: ''},
    depart_date_and_time: {type: String, default: ''},
    move_by: {type: String, default: ''},
    additional_description: {type: String, default: ''},
    created_at: {type: String, default: ''},
    updated_at: {type: String, default: ''}
});

TripPlanSchema.virtual('bid_info',{
    ref: 'bidders',
    localField: '_id',
    foreignField: 'Traveller_id',
    default: {},
    justOne: true
});

TripPlanSchema.virtual('category_info',{
    ref: 'categories',
    localField: 'category_id',
    foreignField: '_id',
    default: {},
    justOne: true
});

TripPlanSchema.set('toObject', { virtuals: true });
TripPlanSchema.set('toJSON', { virtuals: true });

var TripPlan = mongoDB[process.env.DB_NAME].model(table, TripPlanSchema, table);
module.exports.TripPlan = TripPlan;