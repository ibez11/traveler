'use strict';
var {mongoDB} = require("./Db");
var table = 'chatTravellers';

var Schema = mongoDB.Schema;
var ChatTravellerSchema = new Schema({
    Traveller_id: {type: Schema.Types.ObjectId, default: null},
    from_id: {type: Schema.Types.ObjectId, default: null},
    to_id: {type: Schema.Types.ObjectId, default: null},
    message: {type: String, default: ''},
    created_at: {type: String, default: ''}
});

var ChatTraveller = mongoDB[process.env.DB_NAME].model(table, ChatTravellerSchema, table);
module.exports.ChatTraveller = ChatTraveller;