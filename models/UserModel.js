'use strict';
const table = 'users';
const {mongoDB} = require('./Db')

mongoDB.set('useCreateIndex', true);
var Schema = mongoDB.Schema;
var CurrentSchema = new Schema({
    fullname: {type: String, default: ''},
    no_hp: {type: String, default: ''},
    username: {type: String, index: true, unique: true},
    password: {type: String, default: ''},
    user_type: {type: String, default: 'user'},
    is_actived: {type: Boolean, default: true},
});

var CurrentModel = mongoDB[process.env.DB_NAME].model(table, CurrentSchema, table);
module.exports.User = CurrentModel;