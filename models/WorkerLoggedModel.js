'use strict';
var table = 'workerLogged';
var {mongoDB} = require("./Db");

var Schema = mongoDB.Schema;
var workerLoggedSchema = new Schema({
    created_at: String,
    jwt_encrypt: String,
    role_type: Number,
    user_id: {type: Schema.Types.ObjectId},
    username: String
});

workerLoggedSchema.virtual('worker',{
    ref: 'workers',
    localField: 'user_id',
    foreignField: '_id',
    default: {}
});

workerLoggedSchema.set('toObject', { virtuals: true });
workerLoggedSchema.set('toJSON', { virtuals: true });

var workerLogged = mongoDB[process.env.DB_NAME].model(table, workerLoggedSchema, table);
module.exports.workerLogged = workerLogged;