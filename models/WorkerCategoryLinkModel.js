'use strict';
const table = 'workerCategoryLinks';
const {mongoDB} = require('./Db')

mongoDB.set('useCreateIndex', true);
var Schema = mongoDB.Schema;
var CurrentSchema = new Schema({
    category_id: {type: Number, default: null},
    worker_id: {type: Schema.Types.ObjectId, default: null}
});

CurrentSchema.virtual('category_detail',{
    ref: 'categories',
    localField: 'category_id',
    foreignField: '_id'
});

CurrentSchema.set('toObject', { virtuals: true });
CurrentSchema.set('toJSON', { virtuals: true });

var CurrentModel = mongoDB[process.env.DB_NAME].model(table, CurrentSchema, table);
module.exports.WorkerCategoryLink = CurrentModel;