'use strict';
var {mongoDB} = require("./Db");
var table = 'bidders';

var Schema = mongoDB.Schema;
var BidderSchema = new Schema({
    Traveller_id: {type: Schema.Types.ObjectId, default: null},
    worker_id: {type: Schema.Types.ObjectId, default: null}
});

var Bidder = mongoDB[process.env.DB_NAME].model(table, BidderSchema, table);
module.exports.Bidder = Bidder;