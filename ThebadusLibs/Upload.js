const fs = require('fs')
const RandomString = require("../lib/Modules/RandomString");

require('dotenv').config();

class Upload {
    constructor(image) {
        this.image = this.uploadImage(image);
    }

    async uploadImage(image) {
        var result = [];
        
        try {
            var prom = await new Promise(async (resolve, reject) => {
                if (!image) {
                    reject({
                        status: false,
                        message: 'Tidak ada gambar / Video.'});
                }

                var randomString = new RandomString();
                
                var fileNameArray = [];

                var imageData = Object.keys(image);
                await Promise.all(
                    imageData.map(async (x) => {
                        var buf = Buffer.from(image[x].data, 'base64');
                        var path = './img/'
                        var fileName = `rek-ber_${Date.now()}${randomString.makeString(4)}.png`;
                        await fs.writeFile(`${path}${fileName}`, buf, 'binary', function(err) {
                            if(err)
                                reject({
                                    status: false,
                                    message: 'No image file'});
         
                        });
                        fileNameArray.push(fileName);
                    })
                )
                
                resolve({
                    status: true,
                    message: 'Telah berhasil tersimpan.',
                    filename: fileNameArray})
            });
        
            result['status'] = prom.status;
            result['message'] = prom.message;
            result['filename'] = prom.filename;
        
      } catch(e) {
        result['status'] = false;
        result['message'] = e.message;
      }
      
      return result;
    }
}

module.exports = Upload;