var mongoose = require("mongoose");
// var Users = require('./database/seeds/users.seeder');
var Categories = require('./database/seeds/categories.seeder');

const mongoURL = process.env.MONGO_URL || 'mongodb://localhost:27017/hris?authSource=admin';

/**
 * Seeders List
 * order is important
 * @type {Object}
 */
module.exports.seedersList = {
  // Users,
  Categories,
};
/**
 * Connect to mongodb implementation
 * @return {Promise}
 */
module.exports.connect = async () => {
  await mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true });
}
  
/**
 * Drop/Clear the database implementation
 * @return {Promise}
 */
module.exports.dropdb = async () => {
  mongoose.connection.db.dropDatabase();
} 
