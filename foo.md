FORMAT: 1A

# API - Traveller
Marketplace API ini mempunyai 2 level user, diantaranya:

1. Admin
2. User (seller & Buyer)

## API Traveller

# Group Register
Group Register ini adalah untuk daftar sebagai user atau worker.

## Register User [/api/register]

### Register User [POST]

+ Request (application/json)
    
    + Body

            {
				"username": "ibez11@gmail.com",
				"password": "qwerty"
			}
+ Response 200

        {
			"data": [],
			"_meta": [],
			"is_error": false,
			"errors": [
				"Anda Berhasil Register."
			],
			"status": 200,
			"message": "Anda Berhasil Register.",
			"trace": []
		}

# Group Auth
Group Auth ini adalah awal dari semua akses API dan mendapatkan token. Token ini berguna untuk akses semua API wajib menyertakan token.

## Login User [/login]

### Token User [POST]

+ Request (application/json)
    
    + Body

            {
				"username": "ibez11@gmail.com",
				"password": "qwerty"
			}
+ Response 200

        {
            "data": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImMwMzEwZGQ0OWMyNWQ0ODVlZDE4OTdlMTNmNWI3NTQ1YTA2ODA0NmMifQ.eyJjaGVjayI6dHJ1ZSwidXNlcl9pZCI6IjVkZTQ4N2IyNTA2NWYwMzkwMGUxZmYwMCIsImlwIjoiMTI3LjAuMC4xIiwiaWF0IjoxNTc1MjU4MDY5LCJleHAiOjE2MDYwMTY0Njl9.URYPUbcOYFJwa788w1ExvIzRG5QrmEBmB5d9gJKC38SQsI8rmSiDslxX0yqJkB_xPZ8A1k6RHHFLOECrTzS3DbsXP8smbacTWKGe3KpnNbJHwjdPdgTgBxN1vbG2zSe6lMDFBl1PXNLM6vcRghzXPw-PiMZSlXbgirZ7EuEYfDIh9SSdDnEf4ZIh2YXEDYleByCKuGQ8-SQSYjKPMb-ST3StEL8D-HWLKKvFdPjGhTOOo7K3AxWxpVPV737LW_dRXXU2gYzB5BV2n9JMTxm-2ZboD8rtROlK9goOohGgmmQfMC95Y67uNw26luDwfQs3j4snZg_19Gg4a23u0bmSdQ",
            "_meta": [],
            "is_error": true,
            "errors": [
                "Anda berhasil login"
            ],
            "status": 200,
            "message": "Anda berhasil login",
            "trace": []
        }

# Group Master Data
Group Master Data:

1. List Tipe Produk.
2. List Tipe Role.
3. List Penanggung Jawab Fee.

## List Tipe Produk [/master/product-type/list{?api_token}{&page}{&per_page}{&keyword}]

### Produk Tipe List [GET]

+ Parameters

	+ page (number, optional) - all untuk munculkan semua atau page yang ingin kita tuju contoh 1,2 dst
	+ per_page (number, optional) - ini sesuai berapa yang ingin kita tampilkan
		+ Default: `15`
	+ keyword (optional) - pencarian
		
+ Response 200

        {
			"data": [
				{
					"_id": 4,
					"name": "fisik",
					"label": "Fisik",
					"notes": "",
					"group_by": "product_type",
					"created_at": "",
					"updated_at": "",
					"__v": 0
				},
				{
					"_id": 5,
					"name": "digital",
					"label": "Digital",
					"notes": "",
					"group_by": "product_type",
					"created_at": "",
					"updated_at": "",
					"__v": 0
				}
			],
			"_meta": {
				"current_page": 1,
				"last_page": 1,
				"per_page": 15
			},
			"is_error": false,
			"errors": [],
			"status": 200,
			"trace": []
		}

## List Tipe Role [/master/role-type/list{?api_token}{&page}{&per_page}{&keyword}]

### Produk Tipe List [GET]

+ Parameters

	+ page (number, optional) - all untuk munculkan semua atau page yang ingin kita tuju contoh 1,2 dst
	+ per_page (number, optional) - ini sesuai berapa yang ingin kita tampilkan
		+ Default: `15`
	+ keyword (optional) - pencarian
		
+ Response 200

        {
			"data": [
				{
					"_id": 2,
					"name": "buyer",
					"label": "Buyer",
					"notes": "",
					"group_by": "role_type",
					"created_at": "",
					"updated_at": "",
					"__v": 0
				},
				{
					"_id": 3,
					"name": "seller",
					"label": "Seller",
					"notes": "",
					"group_by": "role_type",
					"created_at": "",
					"updated_at": "",
					"__v": 0
				}
			],
			"_meta": {
				"current_page": 1,
				"last_page": 1,
				"per_page": 15
			},
			"is_error": false,
			"errors": [],
			"status": 200,
			"trace": []
		}
		
## Penanggung jawab Fee List [/master/Traveller-fee-liable/list{?api_token}{&page}{&per_page}{&keyword}]

### Penanggung jawab Fee List [GET]

+ Parameters

	+ page (number, optional) - all untuk munculkan semua atau page yang ingin kita tuju contoh 1,2 dst
	+ per_page (number, optional) - ini sesuai berapa yang ingin kita tampilkan
		+ Default: `15`
	+ keyword (optional) - pencarian
		
+ Response 200

        {
			"data": [
				{
					"_id": 7,
					"name": "buyer",
					"label": "Buyer",
					"notes": "",
					"group_by": "Traveller_fee_liable",
					"created_at": "",
					"updated_at": "",
					"__v": 0
				},
				{
					"_id": 8,
					"name": "seller",
					"label": "Seller",
					"notes": "",
					"group_by": "Traveller_fee_liable",
					"created_at": "",
					"updated_at": "",
					"__v": 0
				},
				{
					"_id": 9,
					"name": "percentage",
					"label": "percentage",
					"notes": "",
					"group_by": "Traveller_fee_liable",
					"created_at": "",
					"updated_at": "",
					"__v": 0
				}
			],
			"_meta": {
				"current_page": 1,
				"last_page": 1,
				"per_page": 15
			},
			"is_error": false,
			"errors": [],
			"status": 200,
			"trace": []
		}

## Balance User atau Admin [/my-balance]

### Balance User atau Admin [GET]

+ Response 200

        {
			"data": {
				"saldo": 100000
			},
			"_meta": [],
			"is_error": false,
			"errors": [
				"Saldo anda 100000"
			],
			"status": 200,
			"message": "Saldo anda 100000",
			"trace": []
		}


# Group Transaksi
Group Transaksi User ini saya bagi menjadi 3 bagian sbb:

1. List Transaksi Default Waiting di POSTING.
2. Detail Transaksi yang di POSTING.
3. Membuat Transaksi.

Untuk status Transaksi berikut Kode Status

1. STATUS_WAITING = 28;
2. STATUS_CANCEL = 29;
3. STATUS_PROGRESS = 30;
4. STATUS_COMPLETE = 31;
5. STATUS_DISPUTE = 32;

untuk case DISPUTE saya bagi jadi 2 
	- PROGRESS
	- COMPLETE
tetapi di status utama tetap DISPUTE walaupun di status sudah COMPLETE. Untuk menandakan bahwa Transaksi ini pernah bermasalah.

Keterangan Untuk Field "who_pays_Traveller_fee" itu berada pada API Role Type

7. buyer,Buyer,,Traveller_fee_liable
8. seller,Seller,,Traveller_fee_liable
9. percentage,percentage,,Traveller_fee_liable

Keterangan Untuk Field "product_type" itu berada pada API Product Type

4. fisik,Fisik,,product_type
5. digital,Digital,,product_type

Dan Untuk yang lain SILAHKAN LIHAT CATEGORY.CSV

## Traveller List [/my-transaction{?api_token}{&page}{&per_page}{&keyword}]

### Traveller List [GET]

+ Parameters

    + api_token - yang didapat saat login
	+ page (number, optional) - all atau page yang ingin kita tuju contoh 1,2 dst
	+ per_page (number, optional) - ini sesuai berapa yang ingin kita tampilkan
		+ Default: `15`
	+ keyword (optional) - pencarian Traveller
	+ status (optional) - Status Transaksi
		
+ Response 200

        {
		"data": [
			{
				"_id": "5e05ba18121051355411d09f",
				"buyer_email": "dsfdfg@gmail.com",
				"seller_email": "ibez11@gmail.com",
				"role_type": 3,
				"transaction_title": "dsfsfsdg",
				"seller_phone_number": "",
				"who_pays_Traveller_fee": 1,
				"product_type": 1,
				"description": "",
				"file_upload_1": null,
				"file_upload_2": null,
				"file_upload_3": null,
				"file_upload_4": null,
				"file_upload_5": null,
				"Traveller_fee": 500,
				"seller_comment": null,
				"status_id": 28,
				"status_name": "Waiting",
				"price": 10000,
				"length_inspection_period": 12,
				"created_at": "2019/12/27 15:00:24",
				"created_by": "ibez11@gmail.com",
				"updated_at": ""
			},
			{
				"_id": "5e05b63c306f7a39c8fc96e6",
				"buyer_email": "dsfdfg@gmail.com",
				"seller_email": "ibez11@gmail.com",
				"role_type": 3,
				"transaction_title": "dsfsfsdg",
				"seller_phone_number": "",
				"who_pays_Traveller_fee": 1,
				"product_type": 1,
				"description": "",
				"file_upload_1": null,
				"file_upload_2": null,
				"file_upload_3": null,
				"file_upload_4": null,
				"file_upload_5": null,
				"Traveller_fee": 500,
				"seller_comment": null,
				"status_id": 28,
				"status_name": "Waiting",
				"price": 10000,
				"length_inspection_period": 12,
				"created_at": "2019/12/27 14:43:56",
				"created_by": "ibez11@gmail.com",
				"updated_at": ""
			}
		],
		"_meta": {
			"current_page": 1,
			"last_page": 1,
			"per_page": 15
		},
		"is_error": false,
		"errors": [],
		"status": 200,
		"trace": []
	}

## Transaksi Detail [/transaction/detail/:id{?api_token}]

### Transaksi Detail [GET]

+ Parameters

    + api_token - yang didapat saat login
	+ :id - ID Transaksi
		
+ Response 200

        {
			"data": {
				"_id": "5e05b63c306f7a39c8fc96e6",
				"buyer_email": "dsfdfg@gmail.com",
				"seller_email": "ibez11@gmail.com",
				"role_type": 3,
				"transaction_title": "dsfsfsdg",
				"seller_phone_number": "",
				"who_pays_Traveller_fee": 1,
				"product_type": 1,
				"description": "",
				"file_upload_1": null,
				"file_upload_2": null,
				"file_upload_3": null,
				"file_upload_4": null,
				"file_upload_5": null,
				"Traveller_fee": 500,
				"seller_comment": null,
				"status_id": 28,
				"status_name": "Waiting",
				"price": 10000,
				"length_inspection_period": 12,
				"created_at": "2019/12/27 14:43:56",
				"created_by": "ibez11@gmail.com",
				"updated_at": ""
			},
			"_meta": [],
			"is_error": false,
			"errors": [],
			"status": 200,
			"trace": []
		}
		
## Transaksi Create - User [/transaction/:role/create{?api_token}]

### Transaksi Create - User [POST]

+ Parameters

    + api_token - yang didapat saat login
	+ :role - Role yang mau dipilih (Seller atau Buyer)
	

+ Request (application/json)
    
    + Body

            {
                "product_category":"2"
				"transaction_title":"Beli Domain"
				"price":"10000"
				"buyer_email":"dsfdfg@gmail.com"
				"length_inspection_period":"12"
				"who_pays_Traveller_fee":"8 "
				"product_type":"4"
				"description": ""
				"file_upload_1":"image_1.jpg"
				"file_upload_2":"image_1.jpg"
				"file_upload_3":"image_1.jpg"
				"file_upload_4":"image_1.jpg"
				"file_upload_5":"image_1.jpg"
            }
		
+ Response 200

        {
			"data": "5e05ba18121051355411d09f",
			"_meta": [],
			"is_error": false,
			"errors": [
				"Beli Domain telah berhasil tersimpan."
			],
			"status": 200,
			"message": "Beli Domain telah berhasil tersimpan.",
			"trace": []
		}

## Transaksi Set Status In Seller - Admin [/transaction/waiting/set-status/:id{?api_token}]

### Transaksi Set Status In Seller - Admin [POST]

+ Parameters

    + api_token - yang didapat saat login
	+ :id - id transaksi dalam field "_id"
	+ status - By name Contoh ('in_seller')
	

+ Request (application/json)
    
    + Body

            {
                "status": "in_seller"
			}
		
+ Response 200

        {
			"data": [],
			"_meta": [],
			"is_error": false,
			"errors": [
				"Transaksi berhasil di In Seller"
			],
			"status": 200,
			"message": "Transaksi berhasil di In Seller",
			"trace": []
		}

## Transaksi Set Status Progress - Seller [/transaction/progress/set-status/:id{?api_token}]

### Transaksi Set Status Progress - Seller [POST]

+ Parameters

    + api_token - yang didapat saat login
	+ :id - id transaksi dalam field "_id"
	+ status - By name Contoh ('progress')
	+ comment_seller - Komen Seller
	

+ Request (application/json)
    
    + Body

            {
                "status": "progress"
				"comment_seller": "Nomor Resi Berikut"
			}
		
+ Response 200

        {
			"data": [],
			"_meta": [],
			"is_error": false,
			"errors": [
				"Transaksi berhasil di Progress"
			],
			"status": 200,
			"message": "Transaksi berhasil di Progress",
			"trace": []
		}
		
## Transaksi Set Status Complete - Buyer [/transaction/buyer/set-status/:id{?api_token}]

### Transaksi Set Status Complete - Buyer [POST]

+ Parameters

    + api_token - yang didapat saat login
	+ :id - id transaksi dalam field "_id"
	+ status - By name Contoh ('cancel','complete','dispute')
	

+ Request (application/json)
    
    + Body

            {
                "status": "complete"
			}
		
+ Response 200

        {
			"data": [],
			"_meta": [],
			"is_error": false,
			"errors": [
				"Transaksi sudah selesai"
			],
			"status": 200,
			"message": "Transaksi sudah selesai",
			"trace": []
		}

## Transaksi Set Status Dispute - Buyer [/transaction/buyer/set-status/:id{?api_token}]

### Transaksi Set Status Dispute - Buyer [POST]

+ Parameters

    + api_token - yang didapat saat login
	+ :id - id transaksi dalam field "_id"
	+ status - By name Contoh ('complete')
	

+ Request (application/json)
    
    + Body

            {
                "status": "dispute"
				"reason": "Barang tidak sesuai"
			}
		
+ Response 200

        {
			"data": [],
			"_meta": [],
			"is_error": false,
			"errors": [
				"Transaksi dalam sengketa"
			],
			"status": 200,
			"message": "Transaksi dalam sengketa",
			"trace": []
		}
		
# Group Transaksi DISPUTE
Group Transaksi DISPUTE yang berperan disini ada 3 User SBB:

1. Seller.
2. Buyer.
3. Admin.

Untuk status DISPUTE berikut Kode Status

1. STATUS_PROGRESS = 30.
2. STATUS_COMPLETE = 31.

## Dispute Send Conversation - Seller, Buyer & Admin [/transaction/dispute/send-conversation/:id]

### Dispute Send Conversation - Seller, Buyer & Admin [POST]

+ Parameters

    + api_token - yang didapat saat login
	+ :id - id transaksi dalam field "_id"
	+ message - Pesan Conversation
	

+ Request (application/json)
    
    + Body

            {
                "message": "Blaa..bla"
			}
		
		
+ Response 200

        {
			"data": [],
			"_meta": [],
			"is_error": false,
			"errors": [
				"Transaksi dalam sengketa"
			],
			"status": 200,
			"message": "Transaksi dalam sengketa",
			"trace": []
		}

## Dispute Set Status - Admin [/transaction/dispute/set-status/:id{?api_token}]

### Dispute Set Status - Admin [POST]

+ Parameters

    + api_token - yang didapat saat login
	+ :id - id transaksi dalam field "_id"
	+ buyer_percent - Persentase untuk potongan saldo rumusnya
	 price - (buyerPercent / 100 * price) = total yang di potong untuk buyer
	+ seller_percent - Persen untuk rupiah keuntungan seller
	

+ Request (application/json)
    
    + Body

            {
                "buyer_percent": "10"
				"seller_percent": "90"
			}
		
+ Response 200

        {
			"data": [],
			"_meta": [],
			"is_error": false,
			"errors": [
				"Transaksi sudah selesai"
			],
			"status": 200,
			"message": "Transaksi sudah selesai",
			"trace": []
		}