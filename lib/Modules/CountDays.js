var count = (param) => {
    var date1 = new Date(param.start_date); 
    var date2 = new Date(param.end_date);

    // To calculate the time difference of two dates 
    var differenceInTime = date2.getTime() - date1.getTime(); 
    
    // To calculate the no. of days between two dates 
    var differenceInDays = differenceInTime / (1000 * 3600 * 24);

    // To calculate the time difference of two dates 
    var differenceInTime = date2.getTime() - date1.getTime(); 
    
    // To calculate the no. of days between two dates 
    var differenceInDays = differenceInTime / (1000 * 3600 * 24); 

    return differenceInDays;
}

module.exports = count;