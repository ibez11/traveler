const moment = require('moment');

class DateFormatter {

    constructor() {
    }

    date(date) 
    {
        let now = date().utcOffset("+07:00").format('YYYY/MM/DD HH:mm:ss');
        let setFormat = `${now}`;

        return setFormat;
    };

    dateSlash(date)
    {
        return moment(date).format('YYYY/MM');
    };

    dateCustom(date, format)
    {
        return moment(date).format(format);
    };

    dateBefore(format, count)
    {
        var d1 = new Date (),
            d2 = new Date ( d1 );
        d2.setMinutes ( d1.getMinutes() - count);
        
        return moment(d2).utcOffset("+07:00").format(format);
    };
};

module.exports = DateFormatter;