const jwt = require('jsonwebtoken');
const User = require("../Traveller/Repository/User");
const Worker = require("../Traveller/Repository/Worker");
const ApiController = require('../controllers/ApiController');
var publicPem = require("../key/public.json");

require('dotenv').config();
class Auth extends ApiController
{
    middleware(param)
    {
        let jsonResponse = this.jResp();
        jsonResponse.setStatus(401)
        jsonResponse.setMessage('You do not have access.');
        jsonResponse.setError(true);
        let data = {
            isAuth: async (req,res,next) => 
            {
                var decoded;
                try {
                    
                    var token = req.query.api_token ? req.query.api_token : req.body.api_token;
                    let remoteIp = req.connection.remoteAddress;
                    remoteIp = remoteIp ? remoteIp.split(':') : [];
                    
                    let ip = remoteIp.length <= 3 ? '127.0.0.1' : remoteIp[3]
                    
                    if(token) {
                        const header64 = token.split('.')[0];
                        const header = JSON.parse(Buffer.from(header64, 'base64').toString('ascii'));
                        let publicPemHeaderKid = publicPem[header.kid];
                        
                        jwt.verify(token, publicPemHeaderKid, { algorithms: ['RS256'] }, async (err, result_decoded) => {
                            
                            if (err) {
                                if(!res.headersSent)
                                res.status(401).json(jsonResponse.getResponse());
                            } else {
                                
                                decoded = result_decoded;
                                
                                var repoUser = new User();
                                var repoWorker = new Worker();

                                var isLogged = {};

                                switch(param[0]) {
                                    case 'user':
                                        isLogged = await repoUser.getUserLogged(decoded);
                                        if(isLogged)
                                        isLogged['_doc'].data = isLogged.user[0];
                                        break;
                                    case 'worker':
                                        isLogged = await repoWorker.getWorkerLogged(decoded);
                                        if(isLogged)
                                        isLogged['_doc'].data = isLogged.worker[0];
                                        break;
                                }
                                
                                if(!isLogged) {
                                    if(!res.headersSent)
                                    res.status(401).json(jsonResponse.getResponse());
                                }
                                
                                if(isLogged && isLogged.jwt_encrypt != token) {
                                    if(!res.headersSent)
                                    res.status(401).json(jsonResponse.getResponse());
                                }

                                if(isLogged && ip.toString() != decoded.ip) {
                                    if(!res.headersSent)
                                    res.status(401).json(jsonResponse.getResponse());
                                }
                                
                                if(decoded.check != false && isLogged && isLogged.jwt_encrypt == token && ip.toString() == decoded.ip) {
                                    isLogged = isLogged.toObject();
                                    
                                    let access = await this.getAccessControl(isLogged.data.user_type).hasAccesses(param);

                                    req.user = decoded;
                                    req.user_info = isLogged.data;
                                    
                                    if(access) {
                                        next();
                                    } else {
                                        if(!res.headersSent)
                                        res.status(401).json(jsonResponse.getResponse());
                                    }
                                    
                                }
                            }
                        });
                    } else {
                        if(req.params.login) {
                            next();
                        } else {
                            if(!res.headersSent)
                            res.status(401).json(jsonResponse.getResponse());
                        }
                    }
                    
                } catch(e) {
                    console.log(e)
                    if(!res.headersSent)
                    res.status(401).json(jsonResponse.getResponse());
                }
            }
        }

        return data;
    }
}

module.exports.Auth = new Auth();