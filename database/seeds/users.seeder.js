var { Seeder } = require('mongoose-data-seed');
var faker = require('faker');
const {ObjectId} = require('mongodb'); // or ObjectID
const Model = require("../../models/UserModel").User;
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');

let data = [];

class UsersSeeder extends Seeder {

    async shouldRun() {
        // Ini berjalan apabila data kosong
        return Model.deleteMany({}).exec();
    }
  
    async run() {
        const dateFormatter = new DateFormatter();
        const now = dateFormatter.date(date);

        let count = 1;
        let parent = [];
        let divided = parseInt(count) / 2;
        let b = 0;

        data.push({
            _id: ObjectId(),
            username: 'admin',
            full_name: 'admin',
            gender: '1',
            email: 'admin@gmail.com',
            level: '',
            address: 'Jalan jalan',
            role_type: 2,
            password: "$2b$10$NWn5T79me4pLm0bJddaPLuOwIxWek35nV0qdOMDfE2b2MdGbYaP.O",
            is_actived: true,
            approved: true,
            created_at: now
        },{
            _id: ObjectId(),
            username: 'operator',
            full_name: 'Operator',
            gender: '1',
            email: 'operator@gmail.com',
            level: '',
            address: 'Jalan Hang Jebat 1 No 9',
            role_type: 4,
            password: "$2b$10$9J3On3mO37BUF6EWVOTnVuCr/jCa7vO08tE8FaB/Ej2FbzwXYI71W",
            is_actived: true,
            approved: true,
            created_at: now
        })

        return Model.create(data);
    }
}

module.exports = UsersSeeder;