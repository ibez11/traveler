const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../lib/Modules/DateFormatter");
const date = require('moment');
const qs = require('qs');

const Category = require("../Traveller/Repository/Category");

const CategoryModel = require("../models/CategoryModel").Category;

const CategoryFinder = require("../Traveller/Repository/Finder/CategoryFinder");

class CategoryController extends ApiController
{
    async getModel(id)
    {
        let row = await CategoryModel.findOne({_id: id}).exec();

        if(!row)
            throw new Error('Category tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new CategoryFinder();
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {    
                        finder.orderBy('created_at', 'desc');
                    }
                    
                    var paginator = await finder.get();
                    
                    this.jsonResponse.setData(paginator.data);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    
                    this.jsonResponse.setData(result);

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse())
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                    
                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());
                }
            },
            store: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    req = qs.parse(req);
                    
                    let model = new CategoryModel;
        
                    // Get Id
                    let id = req.params.id;
                    
                    if(id) {
                        model = await CategoryModel.findOne({_id: id}).exec();
                    }

                    let repo = new Category(model);

                    model.name = req.body.name;
                    model.label = req.body.label;
                    model.notes = req.body.notes;
                    model.group_by = 'kategory';

                    model.created_at = !id ? now : undefined;
                    model.updated_at = id ? now : undefined;
                    
                    let result = await repo.save();
                    
                    this.jsonResponse.setMessage(`${result['data'].label} telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            destroy: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;
                    
                    let row = await this.getModel(id);
                    let repo = new Category(row);
                    await repo.deleteOne({_id: id});
                    
                    let message = `${row.name} berhasil dihapus`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let departement = new CategoryController();
module.exports.CategoryController = departement.controller();