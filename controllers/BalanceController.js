const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../lib/Modules/DateFormatter");
const date = require('moment');
const qs = require('qs');

const BalanceModel = require("../models/BalanceModel").Balance;

class BalanceController extends ApiController
{
    async getModel(username)
    {
        let row = await BalanceModel.findOne({email: username}).exec();

        return row;
    }

    controller()
    {
        return {
            balance: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    var model = await this.getModel(req.user.username);
                    var balance = 0;
                    
                    if(model)
                    balance = model.balance;

                    var message = `Saldo anda `+ balance;
                    
                    this.jsonResponse.setData({saldo: balance});
                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            }
        }
    }
}

let balance = new BalanceController();
module.exports.BalanceController = balance.controller();