const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../lib/Modules/DateFormatter");
const date = require('moment');

const TravellerModel = require("../models/TravellerModel").Traveller;
const BidderModel = require("../models/BidderModel").Bidder;
const ChatTravellerModel = require("../models/ChatTravellerModel").ChatTraveller;

const ChatTravellerFinder = require("../Traveller/Repository/Finder/ChatTravellerFinder");
const ChatTraveller = require("../Traveller/Repository/ChatTraveller");

class ChatTravellerController extends ApiController
{
    async getTravellerModel(id)
    {
        let row = await TravellerModel.findOne({_id: id}).populate({
            path: 'bid_info',
            model: BidderModel
        }).sort({created_at: -1}).exec();

        if(!row)
            throw new Error('Traveller tidak ditemukan');

        return row;
    }

    async getModel(TravellerId)
    {
        let row = await ChatTravellerModel.findOne({Traveller_id: TravellerId}).sort({created_at: -1}).exec();

        if(!row)
            throw new Error('Anda belum pernah Chat');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new ChatTravellerFinder(req.user_info.user_type);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }
                    
                    if(req.params.id)
                        finder.setTraveller(req.params.id);
                    
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        var fromDetail = x.from_detail.length ? x.from_detail[0].fullname : '';
                        var toDetail = x.to_detail.length ? x.to_detail[0].fullname : '';
                        var data = {
                            Traveller_id: x.Traveller_id,
                            from: fromDetail,
                            to: toDetail,
                            message: x.message,
                            created_at: x.created_at
                        }

                        list.push(data);
                    })
                    
                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            sendMessageWorker: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new ChatTravellerModel;
                    
                    // Get Id
                    let id = req.params.id;
                    
                    let row = await this.getTravellerModel(id);

                    let repo = new ChatTraveller(model);

                    model.Traveller_id = ObjectId(id);
                    model.from_id = ObjectId(req.user.user_id);
                    model.to_id = ObjectId(row.user_id);
                    model.message = req.body.message;
                    model.created_at = now;
                    
                    await repo.save();
                    
                    this.jsonResponse.setMessage(`Anda berhasil mengirim pesan.`);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            sendMessageUser: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new ChatTravellerModel;
                    
                    // Get Id
                    let id = req.params.id;
                    
                    await this.getTravellerModel(id);

                    let row = await this.getModel(id)

                    let repo = new ChatTraveller(model);
                    
                    model.Traveller_id = ObjectId(id);
                    model.from_id = ObjectId(req.user.user_id);
                    model.to_id = ObjectId(row.from_id);
                    model.message = req.body.message;
                    model.created_at = now;
                    
                    await repo.save();
                    
                    this.jsonResponse.setMessage(`Anda berhasil mengirim pesan.`);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let chatTraveller = new ChatTravellerController();
module.exports.ChatTravellerController = chatTraveller.controller();