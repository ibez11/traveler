const AccessControl = require("../Traveller/Repository/AccessControl");
const JsonResponse = require("../TheBadusLibs/JsonResponse");

class ApiController
{
    constructor() {
        this.jsonResponse = this.jResp();
    }

    jResp()
    {
        return new JsonResponse();
    }

    getAccessControl(userType)
    {
        if(userType)
            return new AccessControl(userType);

        return null;
    }
}


module.exports = ApiController;