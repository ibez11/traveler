const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../lib/Modules/DateFormatter");
const date = require('moment');
const qs = require('qs');

const Traveller = require("../Traveller/Repository/Traveller");

const TravellerModel = require("../models/TravellerModel").Traveller;
const CategoryModel = require("../models/CategoryModel").Category;
const Status = require("../models/TravellerModel").TravellerStatuses;
const BidderModel = require("../models/BidderModel").Bidder;
const TravellerCommentWorkerModel = require("../models/TravellerCommentWorkerModel").TravellerCommentWorker;

const TravellerFinder = require("../Traveller/Repository/Finder/TravellerFinder");

class TravellerController extends ApiController
{
    async getModel(id)
    {
        let row = await TravellerModel.findOne({_id: id}).populate({
            path: 'bid_info',
            model: BidderModel
        }).populate({
            path: 'category_info',
            model: CategoryModel
        }).exec();

        if(!row)
            throw new Error('Traveller tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new TravellerFinder();
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }

                    if(req.user_info.user_type == 'user') {
                        finder.setUser(req.user.user_id);
                        if(req.query.status)
                            finder.setStatus(req.query.status);
                    } else {
                        finder.setCategory({$in: req.user_info.category_detail});
                        finder.setStatus(Status.STATUS_WAITING);
                        finder.setOneHours();
                    }
                        
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        var data = {
                            _id: x._id,
                            from: x.from,
                            to: x.to,
                            pic: x.pic,
                            note: x.note,
                            available_space: x.available_space,
                            depart_date_and_time: x.depart_date_and_time,
                            move_by: x.move_by,
                            additional_description: x.additional_description,
                            title: x.title,
                            description: x.description,
                            category_id: x.category_id,
                            user_id: x.user_id,
                            status: x.status,
                            budget: x.budget,
                            number_of_worker: x.number_of_worker,
                            is_specific_budget: x.is_specific_budget,
                            transaction_no: x.transaction_no,
                            payment_method: x.payment_method,
                            created_at: x.created_at,
                            updated_at: x.updated_at
                        }

                        data.category = {};

                        if(x.category_detail.length)
                        data.category = {
                            _id: x.category_detail[0]._id,
                            label: x.category_detail[0].label
                        };

                        list.push(data);
                    })
                    
                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            bidWorker: async (req,res) => 
            {
                let finder = new TravellerFinder();
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }
                    
                    finder.setWorker(req.user.user_id);
                    
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        var data = {
                            _id: x._id,
                            from: x.from,
                            to: x.to,
                            pic: x.pic,
                            note: x.note,
                            available_space: x.available_space,
                            depart_date_and_time: x.depart_date_and_time,
                            move_by: x.move_by,
                            additional_description: x.additional_description,
                            title: x.title,
                            description: x.description,
                            category_id: x.category_id,
                            user_id: x.user_id,
                            status: x.status,
                            budget: x.budget,
                            number_of_worker: x.number_of_worker,
                            is_specific_budget: x.is_specific_budget,
                            transaction_no: x.transaction_no,
                            payment_method: x.payment_method,
                            created_at: x.created_at,
                            updated_at: x.updated_at
                        }

                        data.category = {};

                        if(x.category_detail.length)
                        data.category = {
                            _id: x.category_detail[0]._id,
                            label: x.category_detail[0].label
                        };

                        list.push(data);
                    })
                    
                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    
                    let list = {
                        _id: result._id,
                        title: result.title,
                        description: result.description,
                        user: result.user,
                        budget: result.budget,
                        category: {
                            _id: result.category_info._id,
                            label: result.category_info.label
                        },
                        number_of_worker: result.number_of_worker,
                        is_specific_budget: result.is_specific_budget,
                        bidders: result.bidders,
                        created_at: result.created_at,
                        updated_at: result.created_by
                    };

                    this.jsonResponse.setData(list);

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse())
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                    
                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());
                }
            },
            store: async (req,res) => 
            {
                
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new TravellerModel;
                    
                    // Get Id
                    let id = req.params.id;
                    
                    if(id) {
                        model = await TravellerModel.findOne({_id: id}).exec();
                    }

                    let repoTraveller = new Traveller(model);

                    model.title = req.body.title;
                    model.description = req.body.description;
                    model.category_id = req.body.category_id;
                    model.budget = req.body.budget;
                    model.user_id = ObjectId(req.user.user_id);

                    model.created_at = !id ? now : undefined;
                    model.updated_at = id ? now : undefined;
                    
                    let result = await repoTraveller.save();
                    
                    this.jsonResponse.setMessage(`${result['data'].title} telah berhasil tersimpan.`);
                    this.jsonResponse.setData(result['data']._id);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            destroy: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;
                    
                    let row = await this.getModel(id);
                    let repo = new Traveller(row);
                    await repo.deleteOne({_id: id});
                    
                    let message = `${row.title} berhasil dihapus`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            setBid: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;

                    let model = new TravellerModel;
                    let bidderModel = new BidderModel;

                    model = await this.getModel(id);

                    let repo = new Traveller(model);

                    var statusOld = model.status;

                    model.status = Status.STATUS_PROGRESS;

                    bidderModel._id = ObjectId();
                    bidderModel.Traveller_id = id;
                    bidderModel.worker_id = ObjectId(req.user.user_id);

                    await repo.addBidder(bidderModel);
                    await repo.saveBidder(statusOld);
                    
                    let message = `Anda berhasil melakukan penawaran`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            finish: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;

                    let model = new TravellerModel;

                    model = await this.getModel(id);

                    model.status = Status.STATUS_FINISH;

                    let repo = new Traveller(model);

                    await repo.saveFinish();
                    
                    let message = `Traveller berhasil di ubah Selesai`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            commentTravellerWorker: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;

                    let model = new TravellerModel;
                    let TravellerCommentWorkerModel = new TravellerCommentWorkerModel;

                    model = await this.getModel(id);

                    let repo = new Traveller(model);
                    
                    TravellerCommentWorkerModel._id = ObjectId();
                    TravellerCommentWorkerModel.user_id = ObjectId(req.user.user_id);
                    TravellerCommentWorkerModel.worker_id = model.bid_info ? ObjectId(model.bid_info.worker_id) : undefined;
                    TravellerCommentWorkerModel.Traveller_id = ObjectId(id);
                    TravellerCommentWorkerModel.rating = req.body.rating;
                    TravellerCommentWorkerModel.comment = req.body.comment;
                    
                    await repo.addTravellerCommentWorkerModel(TravellerCommentWorkerModel);
                    await repo.saveTravellerCommentWorker();
                    
                    let message = `Anda berhasil memberikan rating`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            setCancelAfteOneHours: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                try {
                    let id = req.params.id;

                    let model = new TravellerModel;

                    model = await this.getModel(id);

                    let repo = new Traveller(model);
                    
                    await repo.canceledAfterOneHour();
                    
                    let message = `Success`;

                    this.jsonResponse.setMessage(message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
        }
    }
}

let traveller = new TravellerController();
module.exports.TravellerController = traveller.controller();