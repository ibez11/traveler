const DateFormatter = require("../lib/Modules/DateFormatter");
const date = require('moment');
const {ObjectId} = require('mongodb'); // or ObjectID
const qs = require("qs");

const ApiController = require('./ApiController');
const User = require("../Traveller/Repository/User");
const Worker = require("../Traveller/Repository/Worker");

const UserModel = require("../models/UserModel").User;
const WorkerModel = require("../models/WorkerModel").Worker;
const WorkerCategoryLinkModel = require("../models/WorkerCategoryLinkModel").WorkerCategoryLink;

class AuthController extends ApiController
{
    controller()
    {
        return {
            signInUser: async (req,res,next) => {

                try {
                    var model= new UserModel;
                    
                    var repo= new User(model);

                    model.username = req.body.username;
                    model.password = req.body.password;
        
                    let remoteIp = req.connection.remoteAddress;
                    remoteIp = remoteIp.split(':');
                    
                    let ip = remoteIp.length <= 3 ? '127.0.0.1' : remoteIp[3];
                    
                    var result= await repo.signIn(ip);
                    
                    this.jsonResponse.setData(result.token);
                    this.jsonResponse.setMessage(result.message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }
                
                return res.status(200).send(this.jsonResponse.getResponse());
            },
            signInWorker: async (req,res,next) => {

                try {
                    var model= new WorkerModel;
                    
                    var repo= new Worker(model);

                    model.username = req.body.username;
                    model.password = req.body.password;
        
                    let remoteIp = req.connection.remoteAddress;
                    remoteIp = remoteIp.split(':');
                    
                    let ip = remoteIp.length <= 3 ? '127.0.0.1' : remoteIp[3];
                    
                    var result= await repo.signIn(ip);
                    
                    this.jsonResponse.setData(result.token);
                    this.jsonResponse.setMessage(result.message);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }
                
                return res.status(200).send(this.jsonResponse.getResponse());
            },
            registerWorker: async (req,res,next) => {
                var model= new WorkerModel;
                let modelWorkerCategoryLink = new WorkerCategoryLinkModel;

                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);

                this.jsonResponse = this.jResp();
                
                try {
                    if(req.body.password != req.body.re_password)
                        throw new Error('Password tidak cocok');
                    
                    var _id = ObjectId();
                    req.body = qs.parse(req.body);
                    var repo= new Worker(model);
                    
                    model._id = _id;
                    model.fullname = req.body.fullname;
                    model.confirmation = req.body.confirmation;
                    model.no_hp = req.body.no_hp;
                    model.username = req.body.username;
                    model.password = req.body.password;
                    model.description = req.body.description;
                    model.created_at = now;
                    
                    var category = req.body.category || [];
                    
                    category.forEach(x => {
                        modelWorkerCategoryLink.worker_id = _id;
                        modelWorkerCategoryLink.category_id = x;

                        repo.addCategoryLink(modelWorkerCategoryLink.toObject());
                    })

                    await repo.save();
                    
                    this.jsonResponse.setMessage(`Anda Berhasil Register.`);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            registerUser: async (req,res,next) => {
                var model= new UserModel;

                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);

                this.jsonResponse = this.jResp();
                
                try {
                    if(req.body.password != req.body.re_password)
                        throw new Error('Password tidak cocok');
                    
                    var _id = ObjectId();
                    
                    var repo= new User(model);
                    
                    model._id = _id;
                    model.fullname = req.body.fullname;
                    model.confirmation = req.body.confirmation;
                    model.no_hp = req.body.no_hp;
                    model.username = req.body.username;
                    model.password = req.body.password;
                    model.created_at = now;
                    
                    await repo.save();
                    
                    this.jsonResponse.setMessage(`Anda Berhasil Register.`);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            }
        }
    }
}

let auth = new AuthController();
module.exports.auth = auth.controller();