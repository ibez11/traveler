const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../lib/Modules/DateFormatter");
const date = require('moment');
const qs = require('qs');

const WorkerModel = require("../models/WorkerModel").Worker;
const WorkerCategoryLinkModel = require("../models/WorkerCategoryLinkModel").WorkerCategoryLink;

const Worker = require("../Traveller/Repository/Worker");

const WorkerFinder = require("../Traveller/Repository/Finder/WorkerFinder");

class WorkerController extends ApiController
{
    async getModel(id)
    {
        var pipeline = [
            {
                $lookup: {
                    from: 'workerCategoryLinks',
                    localField: '_id',
                    foreignField: 'worker_id',
                    as: 'category_link_detail',
                }
            },
            {
                $lookup: {
                    from: 'categories',
                    localField: 'category_link_detail.category_id',
                    foreignField: '_id',
                    as: 'category_detail'
                }
            },
            {$match: {_id: ObjectId(id)}}
        ];
        console.log(pipeline)
        let row = await WorkerModel.aggregate(pipeline).exec();
        
        if(!row.length)
            throw new Error('Worker tidak ditemukan');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new WorkerFinder();
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by)
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                        
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        list.push({
                            _id: x._id,
                            fullname: x.fullname,
                            confirmation: x.confirmation,
                            no_hp: x.no_hp,
                            username: x.username,
                            user_type: x.user_type,
                            password: x.password,
                            category: x.category_detail,
                            description: x.description,
                            is_actived: x.is_actived,
                            created_at: x.is_actived
                        });
                    })
                    
                    list = JSON.stringify(list, function (key, value) {return (value === undefined) ? "" : value});

                    this.jsonResponse.setData(JSON.parse(list));
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            show: async (req,res) => 
            {
                this.jsonResponse = this.jResp();
                
                try {
                    let id = req.params.id;
                    
                    let result = await this.getModel(id);
                    result = result[0];
                    
                    let list = {
                        _id: result._id,
                        fullname: result.fullname,
                        confirmation: result.confirmation,
                        no_hp: result.no_hp,
                        username: result.username,
                        user_type: result.user_type,
                        password: result.password,
                        category_detail: result.category_detail,
                        description: result.description,
                        is_actived: result.is_actived,
                        created_at: result.is_actived
                    };
                    
                    list = JSON.stringify(list, function (key, value) {return (value === undefined) ? "" : value});

                    this.jsonResponse.setData(JSON.parse(list));

                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse())
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                    
                    if(!res.headersSent)
                    res.status(200).send(this.jsonResponse.getResponse());
                }
            },
            store: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new WorkerModel;
                    let modelWorkerCategoryLink = new WorkerCategoryLinkModel;

                    // Get Id
                    let id = req.params._id;
                    
                    if(id) {
                        model = await WorkerModel.findOne({_id: id}).exec();
                    } else {
                        id = ObjectId();
                    }

                    req.body = qs.parse(req.body);
                    
                    let repo = new Worker(model);
                    
                    model._id = id;
                    model.fullname = req.body.fullname;
                    model.confirmation = req.body.confirmation;
                    model.no_hp = req.body.no_hp;
                    model.username = req.body.username;
                    model.user_type = req.body.user_type;
                    model.password = req.body.password;
                    model.description = req.body.description;
                    
                    model.created_at = !id ? now : undefined;
                    
                    repo.addCategoryLink(modelWorkerCategoryLink);
                    
                    var category = req.body.category;
                    
                    category.forEach(x => {
                        modelWorkerCategoryLink.worker_id = id;
                        modelWorkerCategoryLink.category_id = x;

                        repo.addCategoryLink(modelWorkerCategoryLink.toObject());
                    })
                    
                    await repo.save();

                    this.jsonResponse.setMessage(`Data Berhasil Tersimpan.`);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            setStatusIsActived: async (req,res) => 
            {
                this.jsonResponse = this.jResp();

                try {
                    let id = req.params.id;

                    let model = new WorkerModel;
                    
                    model = await WorkerModel.findOne({_id: id}).exec();
                    
                    let repo = new Worker(model);

                    model.is_actived = req.body.is_actived;
                    
                    await repo.setStatus()

                    this.jsonResponse.setMessage(`Anda berhasil mengubah status Worker.`);
                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            }
        }
    }
}

let employee = new WorkerController();
module.exports.WorkerController = employee.controller();